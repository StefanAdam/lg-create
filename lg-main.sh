#!/bin/bash

#### Clean Script for LG-Create after stable will be renamed to LG-Create #### 
#### This file is referenced by LG-Selection ####


source ./modules/traps.sh # Error Cleanup
source ./modules/check.sh # Configuration Check
source ./modules/create.sh # Site Creation
source ./modules/provision.sh # Site Provisioning
source ./modules/dashboard.sh # Compatibility Patch with topdown's VVV dashboard

# while [ "$1" != "" ]; do
#     case $1 in
#         -h | --help )           usage
#                                 exit
#                                 ;;
#         * )                     usage
#                                 exit 1
#     esac
#     shift
# done


## VARIABLE DECLARATION ##

ARGCOUNT=$# # Captures number of arguments passed to script
ARG_1=$1 # Captures first argument to call correct function


function runselected {

	# Check if valid number of arguments passed
	if [ $ARGCOUNT -ne 1 ]; then
		echo "Usage: $0 Option" >&2
		exit 1
	fi

	#Check if valid option "create" was passed

	if [ $ARG_1 == "create" ]; then
		create_task
		exit 0
	fi

	# Check if valid option "provision" was passed
	if [ $ARG_1 == "provision" ]; then
		provision_task
		exit 0
	fi

	# Fail with error code if invalid argument pased
	if [ $ARG_1 != "create" ] && [ "$1" != "provision" ];  then
		echo "$0 $1"
	fi

}


function create_task {
	check
	create
	provision_main
	dashboard
	finish
	return 0
}

function provision_task {
	check
	provision_standalone

	if [ "$?" = "5" ]; then
		tput setaf 1
		echo "Error Code: 5, Action Terminated By User."
		tput sgr0
		return 5

	fi

	dashboard
	finish
	return 0
}


runselected






exit

