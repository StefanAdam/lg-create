#!/bin/bash

#######  VVV SITE PROVISIONING MODULE FOR LG-CREATE ######
### Written By Stefan Adam ####
### 2017-09-15  ###

## Provision_standalone is for manually provisioning sites, it is called from the selection menu ##
## Provision_main is used by other modules to provision a site as it is being created. ##



#### CALLED FROM LG-SELECTION ####

function provision_standalone {

	## Required Modules ##
	## These should be called in calling module, added for redundancy and error-protection ##
	source ./modules/traps.sh # Error Cleanup
	source ./modules/check.sh # Configuration Check

	## End Required Modules

	## Local Variables for the standalone provisioner ##

	local HOSTS="./.hosts"
	local BACK=$PWD
	local HOSTS=
	local SITE=

	## End Local Variables ##


	echo "Specify Site you want to provision"
	echo "Do not add the TLD (ex. longevitygraphics NOT longevitygraphics.dev)"

	read SITE

	HOSTS="$SITE.dev"

	echo "Vagrant should not be up right now."
	read -p "Are you sure that you want to provision $HOSTS? (Y/n) " -n 1 -r
		if [[ ! $REPLY =~ ^[Nn]$ ]]; then
				echo "Starting Provisioner"
				$PWD = BACK
				cd $VVV
				vagrant up
				sleep 1
				vagrant provision --provision-with site-$SITE	
				return 0
		else
			echo #
			echo "Site Provisioning Cancelled"
			return 5
		fi
}

#### CALLED FROM LG-MAIN ####

# VARIABLES FROM OTHER MODULES #
# $SITE Was declared in create.sh 
# $VVV Is sourced from .config which is created in check.sh

function provision_main {
	echo "Vagrant should not be up right now."
	read -p "Do you want to provision this site now? (Y/n) " -n 1 -r
		if [[ ! $REPLY =~ ^[Nn]$ ]]; then
			echo "Starting Provisioner"
			$PWD = BACK # Declared in create.sh
			cd $VVV
			vagrant up
			sleep 1
			vagrant provision --provision-with site-$SITE 
			echo "Provisioning $SITE.dev with $HOSTS complete!"
		else
			echo "Remember to provision the site with vagrant provision --provision-with site-$SITE"
			echo "In order to have it accessible in vagrant"
			echo "Remember to create a file called vvv-hosts that contains a list of all of the hosts"
			echo "E.x "$SITE".dev"
		fi
}



