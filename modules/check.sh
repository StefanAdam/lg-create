#!/bin/bash

#######  CONFIGURATION CHECK AND INIT MODULE FOR LG-CREATE ######
### Written By Stefan Adam ####
### 2017-09-15  ###

## VARIABLES ##
CONFIG="./.config"
CUS=./vvv-custom.yml

source $CONFIG

function check {
	if [ -f $CONFIG ]; then
		config
	else
		touch $CONFIG
		config
	fi

}

function config {
	if [ -z "$VVV" ]; then
		tput setaf 1
		echo "ERROR: PATH NOT DEFINED"
		tput sgr0
		touch $CONFIG
		echo "Specify the path to VVV Folder"
		read PATHSET
		echo "VVV="$PATHSET >> $CONFIG
		source $CONFIG
		config
	else
		CUS="$VVV/vvv-custom.yml"
		echo "$CUS Should be the path to your vvv-custom.yml file."
		echo #
	fi
}