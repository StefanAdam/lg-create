#!/bin/bash

#######  VVV SITE CREATION AUTOMATION MODULE FOR LG-CREATE ######
### Written By Stefan Adam ####
### 2017-09-15  ###

## RUNNING VARIABLES ##

CHK=0
BACK=$PWD

## TEMPORARY FILE VARIABLES ##

LOG="./store.tmp"
HOSTS="./.hosts"

## SITE CREATION VARIABLES ##

SITE="ERROR"
REPO="        repo: https://github.com/JPry/vvv-base.git" #Default Repo
HOST="$SITE.dev"
USER="            admin_user: lg-admin"
PASS="            admin_password: password"
EMAIL="            admin_email: admin@localhost.dev"
TITLE="            title: Example Site"
DBPRE="            db_prefix: wp_"
VERS="            version: 4.8.1"
LOCALE="            locale: en_US"



function create {

	# Create Temp File
	touch $LOG
	touch $HOSTS



	if [ -f $CUS ]; then
		echo #
		echo "Enter site name."
		read SITE
		echo "    $SITE:" >> $LOG
	
		read -p "Use Default Repo? (Y/n) " -n 1 -r
		if [[ ! $REPLY =~ ^[Nn]$ ]]; then
			echo #
			echo "Using repo: https://github.com/JPry/vvv-base.git"
			echo "$REPO" >> $LOG
			else
			echo #
			echo "Specify Repo..."
			read REPO
			echo "        repo: $REPO" >> $LOG
		fi
		
		echo "        hosts:" >> $LOG
		while [ $CHK=0 ]; do 
			echo #
			echo "Specify Hosts..."
			echo #
			read HOST
			echo "            - $HOST" >> $LOG
			echo $HOST >> $HOSTS
		
			read -p "Specify Additional Hosts? (y/N) " -n 1 -r
			echo #
			if [[ ! $REPLY =~ ^[Yy]$ ]]; then
				break
			fi

		done

		echo "        custom:" >> $LOG
		read -p "Customize Admin Username (y/N) (Defaults to lg-admin) " -n 1 -r
		if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			echo "$USER" >> $LOG
		else
			echo #
			echo "Enter Admin Username"
			read USER
			echo "            admin_user: $USER" >> $LOG
		fi

		read -p "Customize Admin Password (y/N) (Defaults to password) " -n 1 -r
		if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			echo "$PASS" >> $LOG
		else
			echo #
			echo "Enter Admin Password"
			read PASS
			echo "            admin_password: $PASS" >> $LOG
		fi

		read -p "Customize Admin Email (y/N) (Defaults to admin@localhost.dev) " -n 1 -r
			if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			echo "$EMAIL" >> $LOG
		else
			echo #
			echo "Enter Admin Email"
			read EMAIL
			echo "            admin_email: $EMAIL" >> $LOG
		fi

		read -p "Customize Website Title (Y/n) (Defaults to 'Example Site') " -n 1 -r
		if [[ ! $REPLY =~ ^[Nn]$ ]]; then
			echo "$TITLE" >> $LOG
		else
			echo #
			echo "Enter Website Title"
			read TITLE
			echo "             title: $TITLE" >> $LOG
		fi

		read -p "Customize Database Prefix (y/N) (Defaults to 'wp_') " -n 1 -r
		if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			echo "$DBPRE" >> $LOG
		else
			echo #
			echo "Enter Database Prefix"
			read DBPRE
			echo "            db_prefix: $DBPRE" >> $LOG
		fi

		read -p "Configure as Multisite (y/N) " -n 1 -r
		if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			echo "            multisite: false" >> $LOG
		else
			echo "            multisite: true" >> $LOG
		fi

		read -p "Activate XipIO (Y/n) " -n 1 -r
		if [[ ! $REPLY =~ ^[Nn]$ ]]; then
			echo "            xipio: true" >> $LOG
		else
			echo "            xipio: false" >> $LOG
		fi

		#echo "$VERS" >> $LOG
		#echo "$LOCALE" >> $LOG

		echo "            plugins:" >> $LOG
		source ./modules/plugins.sh # Source the more accessible plugins module, which has user modified plugin settings.
		#echo "                - worker" >> $LOG

		read -p "Install Additonal Plugins? (y/N) " -n 1 -r
		echo #
		if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			sleep 0
		else
			while [ $CHK=0 ]; do 	
				echo #
				echo "Enter Plugin using Slug Address (Ex. ManageWP Worker = 'worker') "
				echo #
				read PLUGIN
				echo "                - $PLUGIN" >> $LOG
				read -p "Add Another Plugin? y/N" -n 1 -r
				echo #
					if [[ ! $REPLY =~ ^[Yy]$ ]]; then
						break
					fi
			done
		fi

		echo "            themes:" >> $LOG
		source ./modules/themes.sh
		#echo "                - twentyeleven" >> $LOG

		read -p "Install Additonal Themes? (y/N) " -n 1 -r
		echo #

		if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			sleep 0
		else
			while [ $CHK=0 ]; do 	
				echo #
				echo "Enter Theme using Slug Address (Ex. twentyten = 'twentyten')"
				echo #
				read THEME
				echo "                - $THEME" >> $LOG
				read -p "Add Another Theme? (y/N) " -n 1 -r
				echo #
				if [[ ! $REPLY =~ ^[Yy]$ ]]; then
					break
				fi
			done
		fi

		read -p "Delete Default Plugins? (y/N)" -n 1 -r
		if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			echo "            delete_default_plugins: false" >> $LOG
		else
			echo "            delete_default_plugins: true" >> $LOG
		fi

		read -p "Delete Default Themes? (y/N)" -n 1 -r
		if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			echo "            delete_default_themes: false" >> $LOG
		else
			echo "            delete_default_themes: true" >> $LOG
		fi

		read -p "Specify wp_content Repository? (y/N)" -n 1 -r
	
		if [[ ! $REPLY =~ ^[Nn]$ ]]; then
			echo #
			echo "Using Default Configuration"
			# Alternate from VVV_Base Default
			#	echo "		wp_content: https://github.com/jquery/jquery-wp-content.git" >> $LOG
		else
			echo #
			echo "Specify wp_content Repository..."
			read REPO
			echo "            wp_content: $REPO" >> $LOG
		fi

		echo "            wp: true" >> $LOG

		read -p "Specify htdocs Repository? (y/N) " -n 1 -r
	
		if [[ ! $REPLY =~ ^[Nn]$ ]]; then
			echo #
			echo "Using Default Configuration"
			# Alternate from VVV_Base Default
			#	echo "		htdocs: https://github.com/salcode/example-project-w-gitignore.git >> $LOG
		else
			echo #
			echo "Specify htdocs Repository..."
			read REPO
			echo "            htdocs: $REPO" >> $LOG
		fi
		echo #
		read -p "Appending to VVV-Custom.yml, Do you wish to continue? (y/N) " -n 1 -r
		echo #
		
		if [[ ! $REPLY =~ ^[Yy]$ ]]; then
			echo "Aborted."
			exit 130
		else
			echo "Writing $LOG to $CUS"
			cat $LOG >> $CUS
			rm $LOG
			sleep 1
			echo "Sucessfully added an entry to VVV-Custom!"
		fi
		

	else
		tput setaf 1
		echo #
		echo "ERROR: vvv-custom.yml Does not exist"
		tput sgr0
		echo "Creating the VVV-Custom.yml File."
		touch $CUS
		echo "VVV-Custom Created!"
		echo "sites:" >> $LOG
		echo #
		cat $LOG >> $CUS
		rm $LOG
		echo "VVV-Custom Prepared for use."
		
		rm $LOG
		main
	fi
}
