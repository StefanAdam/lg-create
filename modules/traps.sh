#!/bin/bash

#######  ERROR HANDLING AND CLEANUP MODULE FOR LG-CREATE ######
### Written By Stefan Adam ####
### 2017-09-15  ###

trap finish EXIT

function finish {
	# Clean-up in case something goes wrong
	rm -f $LOG
	rm -f $HOSTS
}

