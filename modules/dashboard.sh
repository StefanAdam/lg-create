#!/bin/bash

#######  TOPDOWN COMPATIBILITY MODULE FOR LG-CREATE ######
### Written By Stefan Adam ####
### 2017-09-15  ###

#######  This module makes it so that sites created with lg-create appear in topdown's VVV dashboard ######
##### https://github.com/topdown/VVV-Dashboard #####

function dashboard {
			echo "Making site compatible with Topdown's VVV Custom Dashboard"
			cd $BACK # Was declared in create.sh and modified in provision.sh
			sed r $HOSTS >> "$VVV/www/$SITE/vvv-hosts"
			rm $HOSTS

}