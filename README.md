# VVV Create README

Auto Script for quickly adding to the VVV-Custom.yaml from VVV, and then provisioning the site within VVV.

Compatible with the topdown dashboard.

V 1.0 -- LG-Create

- Added Argument Support
  - Create is now invoked through lg-main.sh create
  - Provision is now invoked through lg-main.sh provision

- Code Structure Revamped
  - Built in Modularity for extensibility purposes
  - Each function is managed through individual module "packages"